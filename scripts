#Load Required Packages

library(tidyverse)#R Studio Engine
library(ggplot2) #Visualisation Engine
library(readxl)#Excel Data Import Package
library(dbplyr)#R Data Manipulation Package

#Data Import Script

library(readxl)
disposableincome_edinburgh <- read_excel(<INSERT FILE PATH HERE>)
View(disposableincome_edinburgh)

#Column Renaming Script

colnames(disposableincome_edinburgh) <- c("year", "dis_income", "yoy_growth", "nom_yoy_growth_perc", "inflation", "diff_growth_inflation", "real_growth_inflation_diff", "adjusted_dis_income", "real_growth_int", "real_yoy_change_dis", "real_yoy_growth_perc")

#Inflation vs Nominal YoY Growth Graphing Script

ggplot(data = disposableincome_edinburgh, aes(x=year)) +
  geom_line(mapping = aes(y=nom_yoy_growth_perc, color="YoY Disposable Income Growth"))+
  geom_line(mapping = aes(y=inflation, color="Yearly Inflation Rate"))+
  scale_color_discrete(name = "Legend")+
  scale_y_continuous(breaks=(-2:6))+
  scale_x_continuous(breaks=(1997:2012))+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
  ylab("Percent")+
  xlab("Year")+
  ggtitle("Edinburgh Disposable Income - Inflation vs YoY Growth")

#Nominal Disposable Income Scatter Plot Chart Script

ggplot(data = disposableincome_edinburgh, aes(x=year, y=dis_income)) +
  geom_line(mapping = aes(),linetype="dashed", color="red")+
  geom_point()+
  scale_x_continuous(breaks=(1997:2012))+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
  geom_text(mapping= aes(y=dis_income, label=dis_income), nudge_y= 1000)+
  ylab("Total (£)")+
  xlab("Year")+
  ggtitle("Edinburgh Disposable Income-1997-2012 (£)")

#Nominal Disposable Income Bar Chart Script

ggplot(data=disposableincome_edinburgh, aes(x=year))+
  geom_col(mapping=aes(y=dis_income, fill=year))+
  scale_x_continuous(breaks=(1997:2012))+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
  geom_text(mapping= aes(y=dis_income, label=dis_income), nudge_y= 1000, size=3)+
  ylab("Total (£)")+
  xlab("Year")+
  ggtitle("Edinburgh Disposable Income - Disposable Income")

#Edinburgh Disposable Income - Disposable Income - Nominal vs Inflation Adjusted-Script

ggplot(data = disposableincome_edinburgh, aes(x=year)) +
  geom_line(mapping = aes(y=dis_income,color="Nominal Disposable Income"), linetype="dashed")+
  geom_line(mapping = aes(y=adjusted_dis_income,color="Inflation Adjusted Disposable Income"), linetype="dashed")+
  geom_point(mapping=aes(y=dis_income))+
  geom_point(mapping=aes(y=adjusted_dis_income))+
  scale_color_discrete(name = "Legend")+
  scale_x_continuous(breaks=(1997:2012))+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
  ylab("Total (£)")+
  xlab("Year")+
  ggtitle("Edinburgh Disposable Income - Disposable Income - Nominal vs Inflation Adjusted")

#£ change in disposable income nominal vs real

ggplot(data = disposableincome_edinburgh, aes(x=year)) +
  geom_line(mapping = aes(y=yoy_growth,color="Nominal"), linetype="dashed")+
  geom_line(mapping = aes(y=real_yoy_change_dis,color="Real"), linetype="dashed")+
  geom_point(mapping=aes(y=yoy_growth))+
  geom_point(mapping=aes(y=real_yoy_change_dis))+
  scale_color_discrete(name = "YoY Change")+
  scale_x_continuous(breaks=(1997:2012))+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
  ylab("Total (£)")+
  xlab("Year")+
  ggtitle("Edinburgh Disposable Income - YoY Change (Nom vs Real, £)")

#Some Calculations On Column Data

avg_dis_income <- mean(disposableincome_edinburgh$dis_income, na.rm=TRUE)
avg_real_change<- mean(disposableincome_edinburgh$real_yoy_change_dis, na.rm=TRUE)
avg_nom_change<- mean(disposableincome_edinburgh$yoy_growth, na.rm=TRUE)
